# Batch script for docker-compose visualizer

This script can be used as cli tool.
It runs the provided shell script of the docker-compose visualization tool in a docker container on your windows host.

See: [Docker-compose Visualization](https://github.com/pmsipilot/docker-compose-viz)

## Getting started

- Clone this repository and add it to your `path` in the environment variables
- Now you can use the script from anywhere in your powershell

```
Usage:
  compose-viz [options]                      

Options:
      --file                         Path to a docker compose file [default: "./docker-compose.yml"]
      --override=OVERRIDE            Tag of the override file to use [default: "override"]
  -o, --output-file=OUTPUT-FILE      Path to a output file (Only for "dot" and "image" output format) [default: "./docker-compose.dot" or "./docker-compose.png"]
  -m, --output-format=OUTPUT-FORMAT  Output format (one of: "dot", "image", "display") [default: "display"]
      --only=ONLY                    Display a graph only for a given services (multiple values allowed)
  -f, --force                        Overwrites output file if it already exists
      --no-volumes                   Do not display volumes
  -r, --horizontal                   Display a horizontal graph
      --ignore-override              Ignore override file
```



