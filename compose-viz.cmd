@ECHO off

:: echo the help options
FOR %%# IN ( "-h" "-help") DO (
	IF /i "%~1" EQU "%%~#"  goto :echoHelp
)

SET CURRENTDIR=%cd%
SET FILENAME=docker-compose.yaml
SET OUTPUT_FORMAT=image
SET FORCE=
SET HORIZONTAL_GRAPH=
SET OUTPUT_FILE= 
SET VOLUMES=
SET IGNORE_OVERRIDE=
SET ONLY=
SET OVERRIDE=

::counter for termination condition of the argumentsParser
SET /A SHIFTER=0

:argumentsParser

  ::filename
  IF /i "%~1" EQU "--file" (
     SET "FILENAME=%~2"
  )
  
  ::force
  IF /i "%~1" EQU "-f" (
     SET "FORCE=--force"
	 goto :onlyOneShift
  )
  
  IF /i "%~1" EQU "--force" (
     SET "FORCE=--force"
	 goto :onlyOneShift
  )
  
  ::outputFormat
  IF /i "%~1" EQU "-m" (
     SET "OUTPUT_FORMAT=-m %~2"
  )
  
  IF /i "%~1" EQU "--output-format" (
     SET "OUTPUT_FORMAT=--output-format %~2"
  )
  
  ::horizontalGraph
  IF /i "%~1" EQU "-r" (
     SET "HORIZONTAL_GRAPH=--horizontal"
	 goto :onlyOneShift
  )  
  
  IF /i "%~1" EQU "--horizontal" (
     SET "HORIZONTAL_GRAPH=--horizontal"
	 goto :onlyOneShift
  )
  
  ::outputPath
  IF /i "%~1" EQU "-o" (
     SET "OUTPUT_FILE=--output-file %~2"
  )
  
  IF /i "%~1" EQU "--output-file" (
     SET "OUTPUT_FILE=--output-file %~2"
  )
  
  ::volumes
  IF /i "%~1" EQU "--no-volumes" (
     SET "VOLUMES=--no-volumes"
	 goto :onlyOneShift
  )
  
  ::irgnore Override
  IF /i "%~1" EQU "--ignore-override" (
     SET "IGNORE_OVERRIDE=--ignore-override"
	 goto :onlyOneShift
  )
  
  ::only
  IF /i "%~1" EQU "--only" (
     SET "ONLY=--only %~2"
  )
  
  ::override
  IF /i "%~1" EQU "--override" (
     SET "OVERRIDE=--override %~2"
  )
   
  
  SHIFT
  :onlyOneShift
  SHIFT
  
  SET /A SHIFTER=%SHIFTER% + 1

  IF %SHIFTER% EQU 8 (
     goto :checkParameters
  )
  goto :argumentsParser


:checkParameters

if exist %FILENAME% (
   goto :imageProcess
) else (
   goto :parametersErrorHandling
)


:imageProcess

@REM ECHO %FILENAME% 
@REM ECHO %FORCE%
@REM ECHO %OUTPUT_FORMAT%
@REM @REM ECHO %HORIZONTAL_GRAPH%
@REM @REM ECHO %OUTPUT_FILE% 
@REM @REM ECHO %VOLUMES%
@REM @REM ECHO %IGNORE_OVERRIDE%
@REM @REM ECHO %ONLY%
@REM @REM ECHO %OVERRIDE%

ECHO Generating docker-compose graph...

docker ps >NUL 2>NUL
IF errorlevel == 1 goto dockerErrorHandling

docker run --rm -it --name dcv -v %CURRENTDIR%:/input pmsipilot/docker-compose-viz render %OUTPUT_FORMAT% %OUTPUT-FILE% %ONLY% %OVERRIDE% %FORCE% %HORIZONTAL_GRAPH% %IGNORE_OVERRIDE% %FILENAME% >NUL 2>NUL
IF errorlevel == 1 goto errorHandling

ECHO Successfully finished!
goto :eof

:errorHandling
ECHO.
ECHO There was an error. Please check that you are in the current directory of your docker-compose. Also check your parameters!
ECHO.
goto :echoHelp

:dockerErrorHandling
ECHO.
ECHO Error! Please confirm that your docker deamon is running.
ECHO.
goto :eof

:parametersErrorHandling
ECHO Error! Please check your parameters.
goto :echoHelp

:echoHelp
ECHO ---------------------------------Help-Information------------------------------------
ECHO.
ECHO Usage:
ECHO   compose-viz [options] [--] [<input-file>]
ECHO.
ECHO.
ECHO Options:
ECHO       --file                         Path to a docker compose file [default: "./docker-compose.yaml"]
ECHO       --override=OVERRIDE            Tag of the override file to use [default: "override"]
ECHO   -o, --output-file=OUTPUT-FILE      Path to a output file (Only for "dot" and "image" output format) [default: "./docker-compose.dot" or "./docker-compose.png"]
ECHO   -m, --output-format=OUTPUT-FORMAT  Output format (one of: "dot", "image", "display") [default: "display"]
ECHO       --only=ONLY                    Display a graph only for a given services (multiple values allowed)
ECHO   -f, --force                        Overwrites output file if it already exists
ECHO       --no-volumes                   Do not display volumes
ECHO   -r, --horizontal                   Display a horizontal graph
ECHO       --ignore-override              Ignore override file
